var noviID;
var height1;
var weight1;
var labels1 = [];
var data1 = [];
var arr =[1,2,3,4];
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke() {
  var stPacienta = document.getElementById("TIP").value;
  
  var name;
  var surname;
  var dateOf;
  var height;
  var weight;
  
  
  switch(stPacienta){
    case "Povprečen":
      name = "Janez";
      surname = "povprečen";
      dateOf = "1994-05-12";
      height ="175";
      weight ="70";
    break;
    case "Športnik":
      name ="Janez";
      surname = "Športnik";
      dateOf ="1985-10-10";
      height ="180";
      weight ="65";
    break;
    case "Predebel":
      name = "Janez";
      surname ="predebel";
      dateOf = "1980-11-30";
      height ="172";
      weight ="100";
    break;
    case "Podhranjen":
      name ="Janez";
      surname = "Podhranjen";
      dateOf = "1991-05-15"
      height ="179";
      weight ="50";
    break;
  }
  height1 = height;
  weight1 = weight;
  	if (!name || !surname || !dateOf || name.trim().length == 0 ||
      surname.trim().length == 0 || dateOf.trim().length == 0) {
		$("#error2").html("<span>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        noviID = data.ehrId;
        var partyData = {
          firstNames: name,
          lastNames: surname,
          dateOfBirth: dateOf,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#error2").html("<span>Uspešno kreiran EHRid za novega uporbnika</span>");
              $("#vnosID2").val(ehrId);
              
            }
          },
          error: function(err) {
          	$("#error2").html("<span>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
myFunction();
}

function myFunction() {
  setTimeout(function(){ 



var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
var dateTime = date+'T'+time;



	
	
	var ehrId = noviID;
	var datumInUra = today;
	var telesnaVisina = height1;
	var telesnaTeza = weight1;
	console.log(ehrId);
	
	
	

	if (!ehrId || ehrId.trim().length == 0) {
		alert("Napaka pri vnosu, popravite zapis!");
	} else {
	  
		var podatki = {
			
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "merilec"
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        alert("Uspešno dodajanje vzorčnega uporabnika!");
      },
      error: function(err) {
      	alert("Prišlo je do napake!");
      }
		});
	}
}, 500);}

	



function ustvariUporabnika() {
	var ime = $("#uIme").val();
	var priimek = $("#uPriimek").val();
  var datumRojstva = $("#uDatum").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#error1").html("<span>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#error1").html("<span>Uspešno kreiran EHRid za novega uporabnika</span>");
              $("#vnosID1").val(ehrId);
              $("#uIme").val("");
              $("#uPriimek").val("");
              $("#uDatum").val("");
            }
          },
          error: function(err) {
          	$("#error1").html("<span>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}
function dodajMeritev() {
  var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
var dateTime = date+'T'+time;

  
  
  
	var ehrId = $("#ID").val();
	var datumInUra = today;
	var telesnaVisina = $("#visina").val();
	var telesnaTeza = $("#teza").val();
	
	

	if (!ehrId || ehrId.trim().length == 0) {
	alert("napaka pri vnosu!");
	} else {
	 
		var podatki = {
			
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza
		   	
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "merilec"
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        alert("Uspešno dodajane meritve!");
      },
      error: function(err) {
      	alert("Prislo je do napake!");
      }
		});
	}
}
function izrisi() {
	var ehrId = $("#exe").val();
	var tip = "telesna teža"

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		alert("Napaka pri vnosu podatkov");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
            "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
            " " + party.lastNames + "'</b>.</span><br/><br/>");
  				if (tip == "telesna teža") {
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight + " " 	+
                      res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#rezultatMeritveVitalnihZnakov").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#rezultatMeritveVitalnihZnakov").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				}
	    	},
	    	error: function(err) {
	    		$("#rezultatMeritveVitalnihZnakov").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}
function graph() {
  
  
	var ehrId = $("#gra").val();
	var tip = "telesna teža";
	  var ctx = $('#myChart');
	  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: labels1,
        datasets: [{
            label: 'Weight in kilos',
            data: data1,
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		alert("Napaka  pri vnosu EhrId!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				alert("Uspešen prikaz!");
  				if (tip == "telesna teža") {
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    			    	 
    				    	
  				        for (var i = 0; i < res.length; i++) {
  				          data1[i] = (res[i].weight);
  				          labels1[i]= (res[i].time);
  				          
  				          console.log(typeof arr);
  				        }
  				        myChart.update();
  				        
    			    	} else {
    			    		alert("Ni podatkov!");
    			    	}
    			    },
    			    error: function() {
    			    	alert("Prišlo je do napake");
    			    }
  					});
  				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}


  
  
  

  
}
function calc() {
  var tez = $("#w").val();
  var vis = $("#h").val();
  var resultat = ((tez)/Math.pow((vis/100),2));
  resultat = (Math.round(resultat * 100) / 100);
  $("#rez").val(resultat);
  
}




// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
